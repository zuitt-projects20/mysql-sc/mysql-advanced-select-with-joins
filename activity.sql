-- Find all artists with the letter d in their name
SELECT * FROM artists WHERE name LIKE "%d%" OR "d%" OR "%d";

-- Find all songs that have a length of less than 230
SELECT * FROM songs WHERE length < 230;

-- Join the albums and songs table, displaying only album name, song name, and song length
SELECT album_title, song_name, length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- Sort the albums in Z-A order, show only the first 4 records

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the albums and songs tables, sort albums from Z-A and songs from A-Z

SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;
